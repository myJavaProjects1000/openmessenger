package it.lundstedt.erik.openMessenger;
import it.lundstedt.erik.menu.Menu;
import it.lundstedt.erik.openMessenger.encryption.Encryption;


public class OpenMessenger {
	public static void main(String[] args) throws Exception {
		Menu.licence("openMessenger", "0.2");
		boolean doDebug = true;
		String[] header = new String[]{"what do you want to start?"};
		String[] menuItems = new String[]{"0)server","1)client"};
		Menu mainMenu=new Menu();
		mainMenu.drawMenu(header,menuItems);
		//System.out.println(Encryption.decrypt("2vhRl68M2Dnfn5g/NKALjQ==","user20"));
		int choice=Menu.getChoice();
		switch (choice){
			case 0:
				Server.main(args);
			break;
			case 1:
				new clientMenu();
			break;
			case 2:
				String encrStr="erilun06"+Encryption.encrypt("hello world","erik");
				System.out.println(encrStr);
				System.out.println(encrStr.substring(0,8));
				System.out.println(Encryption.decrypt(encrStr.substring(8),"erik"));

				break;

		}

	}
}